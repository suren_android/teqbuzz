package com.kaanth.teqbuzz.models;

/**
 * Created by Surendar.V on 27-08-2017.
 */

public class LocalRoute {
    String routeId, vehicleIld, scheduleId, routes;

    public String getVehicleIld() {
        return vehicleIld;
    }

    public void setVehicleIld(String vehicleIld) {
        this.vehicleIld = vehicleIld;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getRoutes() {
        return routes;
    }

    public void setRoutes(String routes) {
        this.routes = routes;
    }
}
