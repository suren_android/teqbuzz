package com.kaanth.teqbuzz.Listeners;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kaanth.teqbuzz.Entities.RouteResponseEntity;

import java.util.ArrayList;

/**
 * Created by Surendar.V on 9/2/2017.
 */

public interface RouteParseListener {
    void onPolyLinesCreated(PolylineOptions polylineOptions, ArrayList<LatLng> points, RouteResponseEntity routeResponseEntity);
}
