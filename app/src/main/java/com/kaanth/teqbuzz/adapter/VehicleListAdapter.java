package com.kaanth.teqbuzz.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdChoicesView;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSettings;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdsManager;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.kaanth.teqbuzz.MainActivity;
import com.kaanth.teqbuzz.R;
import com.kaanth.teqbuzz.fragments.BusMapFragment;
import com.kaanth.teqbuzz.models.Vehicle;
import com.kaanth.teqbuzz.utilities.AppDatabaseHelper;
import com.kaanth.teqbuzz.utilities.Preferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;


/**
 * Created by user on 8/18/2015.
 */
public class VehicleListAdapter extends BaseAdapter {


    Context context;
    LayoutInflater inflater;
    ArrayList<Vehicle> vehicles, mainVehicles = new ArrayList<Vehicle>();
    BusMapFragment busMapFragment;
    private String selectedVehicleId;
    Preferences myPreferences;
    double userLatitude, userLongitude;
    AppDatabaseHelper teqBuzzDatabaseHelper;
    private static String LOG_TAG = "EXAMPLE";

    NativeExpressAdView mAdView;
    VideoController mVideoController;

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    int AD_POSITION = 4;
    private boolean isAdsLoaded, isFavorite;

    private NativeAdsManager mAds;
    private NativeAd mAd = null;

    public VehicleListAdapter(BusMapFragment busMapFragment, final Context applicationContext, ArrayList<Vehicle> vehicles, NativeAdsManager mAds) {
        this.context = applicationContext;
        this.vehicles = vehicles;
        mainVehicles.addAll(mainVehicles);
        this.busMapFragment = busMapFragment;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        myPreferences = new Preferences(this.context.getApplicationContext());
        userLatitude = Double.parseDouble(myPreferences.getUser().getTemp_latitude());
        userLongitude = Double.parseDouble(myPreferences.getUser().getTemp_longitude());
        teqBuzzDatabaseHelper = new AppDatabaseHelper(this.context);
        this.mAds = mAds;

        if (mAds != null && mAds.isLoaded()) {
            mAd = mAds.nextNativeAd();
        } else {
            Log.d("", "");
        }
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        myPreferences = new Preferences(context.getApplicationContext());
    }

    @Override
    public int getCount() {
        return vehicles.size();
    }

    @Override
    public Vehicle getItem(int i) {
        return vehicles.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void setSelectedVehicle(String vehicle_id) {
        selectedVehicleId = vehicle_id;
    }

    public String getSelectedVehicle() {
        return selectedVehicleId;
    }

    public void onAdsLoaded(NativeAd nativeAd) {
    }

    public void sortDistanceWise() {
        /*Collections.sort(vehicles, new Comparator<Vehicle>() {
            @Override
            public int compare(Vehicle p1, Vehicle p2) {
                int distance1 = (int) Double.parseDouble(p1.getDistance());
                int distance2 = (int) Double.parseDouble(p2.getDistance());
                return distance1 - distance2; // Ascending
            }
        });
        //return vehicles;
        ((MainActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });*/
        ((MainActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                vehicles = sort();
                notifyDataSetChanged();
            }
        });

    }

    private ArrayList<Vehicle> sort() {
        Comparator<Vehicle> myComparator = new Comparator<Vehicle>() {
            public int compare(Vehicle obj1, Vehicle obj2) {
                if (obj2.getDistance() == obj1.getDistance()) {
                    return 0;
                } else if (Integer.parseInt(obj2.getDistance()) < Integer.parseInt(obj1.getDistance())) {
                    return 1;
                } else {
                    return -1;
                }
                //return obj2.getDistance() < (obj1.getDistance());
            }
        };

        Collections.sort(vehicles, myComparator);
        return vehicles;
    }

    public void setData(ArrayList<Vehicle> teqBuzzVehicles, String filterText) {
        this.vehicles = teqBuzzVehicles;
        mainVehicles.clear();
        mainVehicles.addAll(busMapFragment.getVehicles());
        if (!filterText.equalsIgnoreCase("-1")) {
            filter(filterText);
        } else {
            notifyDataSetChanged();
        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        vehicles.clear();
        if (charText == null || charText.length() == 0) {
            vehicles.addAll(mainVehicles);
        } else {
            for (Vehicle wp : mainVehicles) {
                if (wp.getVehicle_line_number().toLowerCase(Locale.getDefault()).contains(charText)) {
                    vehicles.add(wp);
                }
            }
        }
        notifyDataSetChanged();
        if (vehicles.size() > 0) {
            busMapFragment.updateVehicleMarkers(null, vehicles);
        }
    }

    public static class Holder {
        RelativeLayout vehicle_list_container;
        TextView next_stop, vehicle_num, now_stop, vehicleDistance, fromStopTV, toStopTV, vehicleAgency;
        ImageView img_icon, favIcon, clearSelected;
        NativeExpressAdView adView;
        int position;
        CardView busCardView;
        RelativeLayout sub_container, click_view;

        /*Ads declaration*/
        CardView adCardView;
        ImageView nativeAdIcon, adCoverImage;
        TextView nativeAdTitle;
        TextView nativeAdBody;
        MediaView nativeAdMedia;
        TextView nativeAdSocialContext;
        Button nativeAdCallToAction;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        final Holder holder;
        Vehicle vehicle = vehicles.get(i);

        if (view == null) {

            holder = new Holder();
            holder.position = i;
            view = inflater.inflate(R.layout.vehicle_list_item, viewGroup, false);

            Animation animation = AnimationUtils.loadAnimation(context, R.anim.push_left_in);
            view.startAnimation(animation);
            holder.fromStopTV = (TextView) view.findViewById(R.id.from_stop);
            holder.toStopTV = (TextView) view.findViewById(R.id.to_stop);
            holder.vehicle_list_container = (RelativeLayout) view.findViewById(R.id.vehicle_list_container);
            holder.busCardView = (CardView) view.findViewById(R.id.bus_card);
            holder.next_stop = (TextView) view.findViewById(R.id.next_stop);
            holder.vehicle_num = (TextView) view.findViewById(R.id.vehicle_num);
            holder.now_stop = (TextView) view.findViewById(R.id.now_stop);
            holder.vehicleDistance = (TextView) view.findViewById(R.id.distance);
            holder.vehicleAgency = (TextView) view.findViewById(R.id.busDistance);
            holder.img_icon = (ImageView) view.findViewById(R.id.vehicle_icon);
            holder.favIcon = (ImageView) view.findViewById(R.id.favIcon);
            holder.clearSelected = (ImageView) view.findViewById(R.id.clear_selected);
            holder.sub_container = (RelativeLayout) view.findViewById(R.id.sub_container);
            holder.click_view = (RelativeLayout) view.findViewById(R.id.click_view);

            // holder.adView = (NativeExpressAdView) view.findViewById(R.id.ad);

            try {
             /*   Picasso.with(context).load(WebService.VEHICLE_IMAGE_URL + vehicles.get(i).getImages()).fit().
                        error(R.drawable.bus_white).into(holder.img_icon);
*/
            } catch (IndexOutOfBoundsException e) {

            }


            /*Ads declaration*/
            holder.adCardView = (CardView) view.findViewById(R.id.ad_card);
            holder.nativeAdIcon = (ImageView) view.findViewById(R.id.native_ad_icon);
            //holder.adCoverImage = (ImageView) view.findViewById(R.id.ad_cover_image);
            holder.nativeAdTitle = (TextView) view.findViewById(R.id.native_ad_title);
            holder.nativeAdBody = (TextView) view.findViewById(R.id.native_ad_body);
            holder.nativeAdMedia = (MediaView) view.findViewById(R.id.native_ad_media);
            holder.nativeAdSocialContext = (TextView) view.findViewById(R.id.native_ad_social_context);
            holder.nativeAdCallToAction = (Button) view.findViewById(R.id.native_ad_call_to_action);
            /*Ads declaration*/

            showNativeAd(holder.adCardView, holder);
            mAdView = (NativeExpressAdView) view.findViewById(R.id.adView);

            // Set its video options.
            mAdView.setVideoOptions(new VideoOptions.Builder()
                    .setStartMuted(true)
                    .build());

            // The VideoController can be used to get lifecycle events and info about an ad's video
            // asset. One will always be returned by getVideoController, even if the ad has no video
            // asset.
            mVideoController = mAdView.getVideoController();
            mVideoController.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                @Override
                public void onVideoEnd() {
                    Log.d(LOG_TAG, "Video playback is finished.");
                    super.onVideoEnd();
                }
            });

            // Set an AdListener for the AdView, so the Activity can take action when an ad has finished
            // loading.
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    if (mVideoController.hasVideoContent()) {
                        Log.d(LOG_TAG, "Received an ad that contains a video asset.");
                    } else {
                        Log.d(LOG_TAG, "Received an ad that does not contain a video asset.");
                    }
                }

                @Override
                public void onAdFailedToLoad(int i) {
                    super.onAdFailedToLoad(i);
                }
            });

            mAdView.loadAd(new AdRequest.Builder().addTestDevice("A5F8279DBC0D32EF888AC288EEA907CC").build());
            view.setTag(holder);

        } else {
            holder = (Holder) view.getTag();
        }

        try {


            holder.vehicle_num.setText(vehicle.getVehicle_line_number());// + " " + String.valueOf(vehicles.get(i).isMoving()));

            holder.vehicleDistance.setText(vehicle.getDistance() + " km");

            holder.vehicleAgency.setText(vehicle.getVehicle_agency());
            if (vehicle.getStart_stop() != null && !vehicle.getStart_stop().equalsIgnoreCase("")) {
                holder.fromStopTV.setText("From - " + vehicle.getStart_stop());
            } else {
                holder.fromStopTV.setText("From - " + "Not available");
            }

            if (vehicle.getEnd_stop() != null && !vehicle.getEnd_stop().equalsIgnoreCase("")) {
                holder.toStopTV.setText("To - " + vehicle.getEnd_stop());
            } else {
                holder.toStopTV.setText("To - " + "Not available");
            }

            if (vehicle.getNext_stop() != null && !vehicle.getNext_stop().equalsIgnoreCase("")) {
                holder.next_stop.setText("Next Stop - " + vehicle.getNext_stop());
            } else {
                holder.next_stop.setText("Next Stop - " + "Not available");
            }

            if (vehicle.getPrev_stop() != null && !vehicle.getPrev_stop().equalsIgnoreCase("")) {
                holder.now_stop.setText("Previous stop - " + vehicle.getPrev_stop());
            } else {
                holder.now_stop.setText("Previous stop - " + "Not available");
            }


            /*if (teqBuzzDatabaseHelper.isVehicleFav(vehicle) && myPreferences.isLoginned()) {
                holder.favIcon.setVisibility(View.VISIBLE);
            } else {
                holder.favIcon.setVisibility(View.GONE);
            }*/

            if (vehicle.isFavourite()/* || teqBuzzDatabaseHelper.isVehicleFav(vehicle)*/) {
                holder.favIcon.setVisibility(View.VISIBLE);
            } else {
                holder.favIcon.setVisibility(View.GONE);
            }
            /*if (vehicle.isSelected() && busMapFragment.getSelectedBusRouteLineLine() != null) {
                holder.clearSelected.setVisibility(View.VISIBLE);
            } else {
                holder.clearSelected.setVisibility(View.GONE);
            }*/

        } catch (IndexOutOfBoundsException e) {

        }


        if (i % AD_POSITION != 0 || i == 0) {
            holder.adCardView.setVisibility(View.GONE);
        } else {
            if (isAdsLoaded)
                holder.adCardView.setVisibility(View.VISIBLE);
            else
                holder.adCardView.setVisibility(View.GONE);
        }


        holder.click_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                busMapFragment.onBusListItemSelcted(i);
            }
        });
        holder.clearSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                busMapFragment.clearSelectedVehicle();
                vehicles.get(i).setSelected(false);
                notifyDataSetChanged();
            }
        });

        return view;
    }


    private View showNativeAd(final View fbAdView, final Holder holder) {
        AdSettings.addTestDevice("ec2b6d183e1a490e81c27eee13f62a1e");
        final NativeAd nativeAd = new NativeAd(context, "2344690405756691_2344703089088756");


        nativeAd.setAdListener(new com.facebook.ads.AdListener() {

            AdChoicesView adChoicesView;

            @Override
            public void onError(Ad ad, AdError error) {
                isAdsLoaded = false;
                Log.e("suren ", error.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if (ad != nativeAd) {
                    return;
                }
                isAdsLoaded = true;
                // Setting the Text.
                holder.nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
                holder.nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
                holder.nativeAdTitle.setText(nativeAd.getAdTitle());
                holder.nativeAdBody.setText(nativeAd.getAdBody());

                // Downloading and setting the ad icon.
                NativeAd.Image adIcon = nativeAd.getAdIcon();
                NativeAd.downloadAndDisplayImage(adIcon, holder.nativeAdIcon);

                // Download and setting the cover image.
                //NativeAd.Image adCoverImage = nativeAd.getAdCoverImage();
                //nativeAd.downloadAndDisplayImage(adCoverImage, holder.adCoverImage);
                //holder.nativeAdMedia.setNativeAd(nativeAd);

                // Add adChoices icon
                if (adChoicesView == null) {
                    adChoicesView = new AdChoicesView(context, nativeAd, true);
                    //fbAdView.addView(adChoicesView, 0);
                }

                nativeAd.registerViewForInteraction(fbAdView);
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }

        });

        nativeAd.loadAd();
        return fbAdView;
    }

    @Override
    public void notifyDataSetInvalidated() {
        //sortDistanceWise();
        super.notifyDataSetInvalidated();
        Log.d("VehicleListAdapter", "notifyDataSetInvalidated()");
        Log.d("Sorting", "notified");

    }

}

