package com.kaanth.teqbuzz.service;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kaanth.teqbuzz.Entities.RouteResponseEntity;
import com.kaanth.teqbuzz.Listeners.RouteParseListener;
import com.kaanth.teqbuzz.R;
import com.kaanth.teqbuzz.utilities.DirectionsJSONParser;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Surendar.V on 9/2/2017.
 */

public class LocalRouteParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

    private PolylineOptions polylineOptions;
    Context context;
    RouteResponseEntity routeResponseEntity;
    RouteParseListener routeParseListener;

    public LocalRouteParserTask(Context context, RouteParseListener routeParseListener, RouteResponseEntity routeResponseEntity) {
        this.context = context;
        this.routeParseListener = routeParseListener;
        this.routeResponseEntity = routeResponseEntity;
    }

    @Override
    protected void onPreExecute() {
        //showProgress();
        super.onPreExecute();
    }

    @Override
    protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

        JSONObject jObject;
        List<List<HashMap<String, String>>> routes = null;

        try {
            jObject = new JSONObject(jsonData[0]);
            DirectionsJSONParser parser = new DirectionsJSONParser();

            // Starts parsing data
            routes = parser.parse(jObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return routes;
    }

    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> result) {
        ArrayList<LatLng> points = null;
        polylineOptions = null;

        // Traversing through all the routes
        if (result != null && result.size() == 0) {
            Log.e("", "");
        }
        if (result != null) {
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                polylineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions

                polylineOptions.addAll(points);
                polylineOptions.width(20);
                polylineOptions.color(context.getResources().getColor(R.color.colorPrimary));


                // setting route paths for vehicle
                //setRoutePathsForSelectedVehicles(points);
                //removeNonSelectedVehicleMarkers();
                for (LatLng point : points) {
                    Log.d("latlngpolyline", "latitude : " + String.valueOf(point.latitude + "longitude : " + String.valueOf(point.longitude)));
                }
                Log.d("latlngpolyline", ".........................................................................................................");

            }

            if (points != null && points.size() > 0) {
                Location location = new Location("");
                location.setLatitude(points.get(0).latitude);
                location.setLongitude(points.get(0).longitude);

            } else {
            }
        }
        routeParseListener.onPolyLinesCreated(polylineOptions, points, routeResponseEntity);
        //googleMap.addPolyline(polylineOptions);

    }

}
