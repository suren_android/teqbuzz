package com.kaanth.teqbuzz.Entities;

import android.util.Log;

import com.kaanth.teqbuzz.models.Stop;
import com.kaanth.teqbuzz.models.Vehicle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Surendar.V on 8/31/2017.
 */

public class RouteResponseEntity {
    String routes, vehicleId, scheduleId;
    ArrayList<Stop> stops;

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getRoutes() {
        return routes;
    }

    public void setRoutes(String routes) {
        this.routes = routes;
    }

    public ArrayList<Stop> getStops() {
        return stops;
    }

    public void setStops(ArrayList<Stop> stops) {
        this.stops = stops;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public static RouteResponseEntity parseJson(String response) {

        RouteResponseEntity routeResponseEntity = null;
        try {
            routeResponseEntity = new RouteResponseEntity();
            JSONObject mainJsonObject = new JSONObject(response);
            if (mainJsonObject.has("msg_type")) {
                String msgType = mainJsonObject.getString("msg_type");
                if (msgType.equalsIgnoreCase("success")) {
                    String routes = mainJsonObject.getString("routes");
                    routeResponseEntity.setRoutes(routes);

                    JSONArray jsonStopsArray = mainJsonObject.getJSONArray("stops");
                    ArrayList<Stop> stops = new ArrayList<Stop>();
                    String stop_id, stop_name, stop_latitude, stop_longitude;
                    for (int i = 0; i < jsonStopsArray.length(); i++) {
                        JSONObject jsonObjectStop = jsonStopsArray.getJSONObject(i);
                        stop_id = jsonObjectStop.getString("stop_id");
                        stop_name = jsonObjectStop.getString("stop_name");
                        stop_latitude = jsonObjectStop.getString("stop_latitude");
                        stop_longitude = jsonObjectStop.getString("stop_longitude");
                        Stop stop = new Stop();
                        stop.setStop_id(stop_id);
                        stop.setStop_name(stop_name);
                        stop.setStop_latitude(stop_latitude);
                        stop.setStop_longitude(stop_longitude);
                        stops.add(stop);
                    }
                    routeResponseEntity.setStops(stops);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return routeResponseEntity;
    }
}
